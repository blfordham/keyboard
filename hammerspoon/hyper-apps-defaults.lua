-- Default keybindings for launching apps in Hyper Mode
--
-- To launch _your_ most commonly-used apps via Hyper Mode, create a copy of
-- this file, save it as `hyper-apps.lua`, and edit the table below to configure
-- your preferred shortcuts.
return {
  { 'b', 'Firefox' },            -- "B" for "Browser"
  { 'd', 'Todoist' },            -- "D" for "Do!" ... or "Done!"
  { 'e', 'Visual Studio Code' }, -- "E" for "Editor"
  { 'g', 'Google Chrome' },      -- "G" for "Google Chrome"
  { 'f', 'Finder' },             -- "F" for "Finder"
  { 'm', 'Spotify' },            -- "M" for "Music"
  { 's', 'Slack' },              -- "S" for "Slack"
  { 't', 'iTerm' },              -- "T" for "Terminal"
}
